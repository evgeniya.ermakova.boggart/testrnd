using UnityEngine;

public static class SaveManager
{
    public static void Save(string key, SaveData saveData)
    {
        var jSonDataString = JsonUtility.ToJson(saveData, true);
        PlayerPrefs.SetString(key, jSonDataString);
    }

    public static SaveData Load(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            var loadedString = PlayerPrefs.GetString(key);
            return JsonUtility.FromJson<SaveData>(loadedString);
        }
        else
            return new SaveData();
    }
}
