using UnityEngine;
using UnityEngine.UI;

public class StartPopup : Popup
{
   [SerializeField] private Text _text;
   private const string TEMPLATE = "Добро пожаловать в {0} уровень!";

   public void Initialize(int level)
   {
      _text.text = string.Format(TEMPLATE, level);
   }
}
