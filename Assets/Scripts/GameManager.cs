using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private const string SAVE_KEY = "mainSave";
    private const int DAMAGE = 10;
    private const string TEMPLATE_LEVEL_TEXT = "Уровень {0}";

    [SerializeField] private HealthPopup _playerHealthPopup;
    [SerializeField] private HealthPopup _enemyHealthPopup;

    [SerializeField] private StartPopup _startPopup;
    [SerializeField] private Popup _winPopup;
    [SerializeField] private Popup _losePopup;
    [SerializeField] private Character _player;
    [SerializeField] private Character _enemy;
    [SerializeField] private Text _levelText;

    private SaveData _gameData;
    private bool _isRuningGame = false;
    private LevelSO[] _levels;

    private void Start()
    {
        _levels = Resources.LoadAll<LevelSO>("Levels/");

        _gameData = SaveManager.Load(SAVE_KEY);

        _startPopup.Initialize(_gameData.Level);
        _startPopup.Show();
        _startPopup.OnClicked = StartGame;

        _player.OnHit = HitPlayer;
        _enemy.OnHit = HitEnemy;
    }

    private void StartGame()
    {
        var level = _levels.FirstOrDefault(level => level.LevelNumber == _gameData.Level);
        if (level == default)
            return;

        _playerHealthPopup.Initialize(_gameData.PlayerHealth);
        _enemyHealthPopup.Initialize(_gameData.EnemyHealth);

        _player.Initialize(level.PlayerAbilityDatas);
        _enemy.Initialize(level.EnemyAbilityDatas);

        _isRuningGame = true;
        _levelText.text = string.Format(TEMPLATE_LEVEL_TEXT, _gameData.Level);
    }

    private void HitEnemy()
    {
        if (!_isRuningGame)
            return;

        _gameData.EnemyHealth -= DAMAGE;
        SaveManager.Save(SAVE_KEY, _gameData);
        _enemyHealthPopup.ReduceHealth(DAMAGE);

        if (_gameData.EnemyHealth <= 0)
            Win();
    }

    private void HitPlayer()
    {
        if (!_isRuningGame)
            return;

        _gameData.PlayerHealth -= DAMAGE;
        SaveManager.Save(SAVE_KEY, _gameData);
        _playerHealthPopup.ReduceHealth(DAMAGE);

        if (_gameData.PlayerHealth <= 0)
            Lose();
    }

    private void Win()
    {
        StopAttack();

        _winPopup.Show();
        _winPopup.OnClicked = StartGame;

        _gameData.SetIncLevel();
        if (_gameData.Level > _levels.Length)
            _gameData.Level = 1;

        SaveManager.Save(SAVE_KEY, _gameData);

        _player.AnimateVictory();
        _enemy.AnimateDie();
    }

    private void Lose()
    {
        StopAttack();

        _losePopup.Show();
        _losePopup.OnClicked = StartGame;

        _gameData.SetStartHealth();
        SaveManager.Save(SAVE_KEY, _gameData);

        _enemy.AnimateVictory();
        _player.AnimateDie();
    }

    private void StopAttack()
    {
        _player.StopAttack();
        _enemy.StopAttack();

        _isRuningGame = false;

        foreach (var magicBall in transform.GetComponentsInChildren<MagicBallBehaviour>())
            Destroy(magicBall.gameObject);
    }
}
