using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{
    [SerializeField] private AbilityType _abilityType;
    [SerializeField] private Character _player;
    
    private Ability _ability;
    private Slider _slider;

    private void Start()
    {
        var button = transform.GetComponent<Button>();
        if (button == default)
            return;

        _slider = transform.GetComponentInChildren<Slider>();
        
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        _ability = SelectAbilityByType(_player.Abilitys);
        _ability?.StartAbility();
    }
    
    private Ability SelectAbilityByType(IEnumerable<Ability> abilitys)
    {
        switch (_abilityType)
        {
            case AbilityType.BallAttackAbility:
                return abilitys.OfType<BallAttackAbility>().First();
            case AbilityType.ShieldAbility:
                return abilitys.OfType<ShieldAbility>().First();
            case AbilityType.FreezeAbility:
                return abilitys.OfType<FreezeAbility>().First();
            case AbilityType.CometAbility:
                return abilitys.OfType<CometAbility>().First();
        }
        return default;
    }

    private void Update()
    {
        if (_slider == default || _ability == default)
            return;
        
        _slider.value = _ability.GetProgressCooldown();
    }
}
