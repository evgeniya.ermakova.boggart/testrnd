using System;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    private bool _flagStop = true;
    private float _timeCount = 0;
    private float _timeMax = 0;
    private bool _autoReset = false;

    public UnityAction OnTimedEnd;

    public void Init(float time, bool autoReset)
    {
        _flagStop = true;
        _autoReset = autoReset;
        _timeMax = time;
        _timeCount = 0;
    }

    public void Launch()
    {
        if (_timeMax > 0)
            _flagStop = false;
    }

    public void Stop() => _flagStop = true;

    void Update()
    {
        if (_flagStop)
            return;

        _timeCount += Time.deltaTime;

        if (_timeCount >= _timeMax)
        {
            if (_autoReset)
                _timeCount = 0;
            else
                _flagStop = true;

            OnTimedEvent();
        }
    }

    void OnTimedEvent() => OnTimedEnd?.Invoke();

    public float GetTimeInSecToEnd => _timeMax - _timeCount;
    public bool IsTimerRunning => !_flagStop;
}
