using System;

[Serializable]
public class SaveData
{
    private const int START_HEALTH = 200;
        
    public int Level;
    public int PlayerHealth;
    public int EnemyHealth;

    public SaveData()
    {
        Level = 1;
        PlayerHealth = START_HEALTH;
        EnemyHealth = START_HEALTH;
    }

    public void SetIncLevel()
    {
        Level++;
        SetStartHealth();
    }

    public void SetStartHealth()
    {
        PlayerHealth = START_HEALTH;
        EnemyHealth = START_HEALTH;
    }
}
