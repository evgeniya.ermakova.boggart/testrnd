using UnityEngine;
using Slider = UnityEngine.UI.Slider;

public class HealthPopup : MonoBehaviour
{
    [SerializeField] private Slider _healthSlider;

    private int _health;
    private int _healthMax;

    public void Initialize(int health)
    {
        _health = health;
        _healthMax = health;
        UpdateHealth();
    }

    public void ReduceHealth(int delta)
    {
        _health -= delta;
        
        if (_health < 0)
            _health = 0;
        
        UpdateHealth();
    }
    
    private void UpdateHealth() => _healthSlider.value = (float)_health / _healthMax;
}
