using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    [SerializeField] private Button _button;
    public UnityAction OnClicked;

    void Start() =>_button.onClick.AddListener(OnClick);

    public void Show() => gameObject.SetActive(true);

    public void Hide() => gameObject.SetActive(false);

    private void OnClick()
    {
        Hide();
        OnClicked?.Invoke();
    }

    private void OnValidate()
    {
        if (_button == default)
            _button = GetComponentInChildren<Button>();
    }
}
