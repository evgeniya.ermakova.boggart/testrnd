using System.Collections.Generic;
using UnityEngine;

public class LevelSO : ScriptableObject
{
  [SerializeField] private int _levelNumber;
  [SerializeField] private List<AbilityData> _playerAbilityDatas = new List<AbilityData>();
  [SerializeField] private List<AbilityData> _enemyAbilityDatas = new List<AbilityData>();
  
  public int LevelNumber => _levelNumber;
  
  public List<AbilityData> PlayerAbilityDatas => _playerAbilityDatas;
  
  public List<AbilityData> EnemyAbilityDatas => _enemyAbilityDatas;
}
