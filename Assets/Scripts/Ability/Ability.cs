using UnityEngine;
using UnityEngine.Events;

public class Ability : MonoBehaviour
{
    protected Animator _animator;
    protected Character _target;

    private float _workTime;
    private float _cooldownTime;
    private Timer _timer;
    private Timer _cooldownTimer;

    protected virtual void Awake()
    {
        _timer = gameObject.AddComponent<Timer>();
        _cooldownTimer = gameObject.AddComponent<Timer>();
        _animator = transform.GetComponent<Animator>();
    }

    public void Initialize(float workTime, float cooldownTime, Character target)
    {
        _workTime = workTime;
        _target = target;
        _cooldownTime = cooldownTime;
    }

    public virtual bool StartAbility()
    {
        if (IsAbility() || IsCooldown())
            return false;

        StartTimer(_timer, _workTime, FinishAbility);
        StartTimer(_cooldownTimer, _cooldownTime, null);

        return true;
    }

    protected virtual void FinishAbility() { }

    public virtual void StopAbility() => _timer.Stop();

    public float GetProgressCooldown() => (_cooldownTime - _cooldownTimer.GetTimeInSecToEnd) / _cooldownTime;

    private bool IsAbility() => _timer.IsTimerRunning;

    private bool IsCooldown() => _cooldownTime > 0 && _cooldownTimer.IsTimerRunning;

    private void StartTimer(Timer timer, float time, UnityAction call)
    {
        timer.Init(time, false);
        timer.Launch();
        timer.OnTimedEnd = call;
    }
}
