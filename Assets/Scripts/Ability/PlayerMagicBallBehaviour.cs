using UnityEngine;

public class PlayerMagicBallBehaviour : MagicBallBehaviour
{
  protected override void OnTriggerEnter(Collider collider)
  {
    if (!collider.CompareTag("Character"))
      return;

    base.OnTriggerEnter(collider);
  }
}
