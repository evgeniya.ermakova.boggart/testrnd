using UnityEngine;

public class CometAbility : Ability
{
  private const string COMET_NAME = "AnimComet";
  
  private GameObject _comet;

  public override bool StartAbility()
  {
    if (!base.StartAbility())
      return false;
    
    _comet = _target.transform.FindRecursive(COMET_NAME).gameObject;
    _comet.SetActive(true);
    _target.Hit();
    _animator.SetTrigger("Attack");

    return true;
  }

  protected override void FinishAbility()
  {
    base.FinishAbility();
    _comet.SetActive(false);
  }

  public override void StopAbility()
  {
    base.StopAbility();
    _comet?.SetActive(false);
  }
}
