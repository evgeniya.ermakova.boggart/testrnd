public class FreezeAbility : Ability
{
  public override bool StartAbility()
  {
    if (!base.StartAbility())
      return false;

    SetStateAnimation(true);
    return true;
  }

  protected override void FinishAbility()
  {
    base.FinishAbility();
    SetStateAnimation(false);
  }

  public override void StopAbility()
  {
    base.StopAbility();
    SetStateAnimation(false);
  }

  private void SetStateAnimation(bool isActive)
  {
    _animator.SetBool("AttackFreezy", isActive);
    _target.SetState("Freeze", isActive);
  }
}
