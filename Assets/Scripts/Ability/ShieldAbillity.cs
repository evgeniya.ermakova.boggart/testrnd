using UnityEngine;

public class ShieldAbility : Ability
{
  private const string SHIELD_NAME = "Shield";
  
  private GameObject _shield;

  protected override void Awake()
  {
    base.Awake();
    _shield = transform.FindRecursive(SHIELD_NAME).gameObject;
    _shield.SetActive(false);
  }

  public override bool StartAbility()
  {
    if (!base.StartAbility())
      return false;
    
    _shield.SetActive(true);
    _animator.SetTrigger("Shield");

    return true;
  }

  protected override void FinishAbility()
  {
    base.FinishAbility();
    _shield.SetActive(false);
  }

  public override void StopAbility()
  {
    base.StopAbility();
    _shield.SetActive(false);
  }
}
