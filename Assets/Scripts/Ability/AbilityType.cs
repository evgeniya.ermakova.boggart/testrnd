public enum AbilityType
{
    BallAttackAbility = 0,
    ShieldAbility = 1,
    FreezeAbility = 2,
    CometAbility = 3
}
