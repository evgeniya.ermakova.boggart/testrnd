using UnityEngine;

public class BallAttackAbility : Ability
{
  private const float MAGICBALL_DELAY = 0.5f;
  private const string START_POSITION_NAME = "MagicBallPlace";
  
  private MagicBallBehaviour _magicBallPrefab;
  private Transform _magicBallPlaceTransform;
  private Timer _magicBallTimer;

  protected override void Awake()
  {
    base.Awake();

    _magicBallPrefab = GetComponentInChildren<MagicBallBehaviour>(true);
    
    _magicBallPlaceTransform = transform.FindRecursive(START_POSITION_NAME);
    
    _magicBallTimer = gameObject.AddComponent<Timer>();
    _magicBallTimer.OnTimedEnd = CreateMagicBall;
  }
  
  public override void StopAbility()
  {
    base.StopAbility();
    _magicBallTimer.Stop();
  }

  protected override void FinishAbility()
  {
    base.FinishAbility();
    
    _animator.SetTrigger("Attack");
    
    _magicBallTimer.Init(MAGICBALL_DELAY, false);
    _magicBallTimer.Launch();
  }

  private void CreateMagicBall()
  {
    _magicBallPrefab.gameObject.SetActive(true);
    
    var magicBall = Instantiate(_magicBallPrefab.gameObject, transform.parent)
      .GetComponent<MagicBallBehaviour>();
    magicBall.transform.position = _magicBallPlaceTransform.position;
    magicBall.AnimateMove(GetEndPositionMagicBall());
    
    _magicBallPrefab.gameObject.SetActive(false);

    StartAbility();
  }

  private Vector3 GetEndPositionMagicBall()
  {
    var targetTransform = _target.transform;
    return targetTransform.position + targetTransform.GetComponent<CapsuleCollider>().center;
  }
}
