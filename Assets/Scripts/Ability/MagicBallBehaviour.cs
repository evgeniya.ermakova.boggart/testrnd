using System.Collections;
using UnityEngine;

public class MagicBallBehaviour : MonoBehaviour
{
  [SerializeField] private float _speed;
  private float _startTime;

  public void AnimateMove(Vector3 endPosition)
  {
    _startTime = Time.time;
    StartCoroutine(MoveTo(transform.position, endPosition));
  }
  
  private IEnumerator MoveTo(Vector3 startPosition, Vector3 endPosition)
  {
    var distance = Vector3.Distance(startPosition, endPosition);

    while (transform.position != endPosition)
    {
      var time = (Time.time - _startTime) * _speed / distance;
      transform.position = Vector3.Lerp(startPosition, endPosition, time);
      yield return null;
    }
  }

  protected virtual void OnTriggerEnter(Collider collider)
  {
    if (!collider.CompareTag("Character") && !collider.CompareTag("Shield"))
      return;

    Destroy(gameObject);
  }
}
