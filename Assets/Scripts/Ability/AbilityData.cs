using System;

[Serializable]
public class AbilityData
{
   public float Time;
   public float CooldownTime;
   public AbilityType AbilityType;
}
