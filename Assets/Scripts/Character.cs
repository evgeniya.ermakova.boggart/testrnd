using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
  [SerializeField] protected Animator _animator;
  [SerializeField] private Character _enemy;

  public UnityAction OnHit;

  private bool isState = false;
  protected List<Ability> _abilitys = new List<Ability>();
  private List<AbilityData> _abilityDatas = new List<AbilityData>();
  private Ability _mainAbility;

  public virtual void Initialize(List<AbilityData> abilityDatas)
  {
    _abilityDatas = abilityDatas;
    _animator.Play("Idle01");

    foreach (var abilityData in _abilityDatas)
      CreateAbility(abilityData);
    
    SetMainAbility(_abilitys.OfType<BallAttackAbility>().First());
    StartMainAttack();
  }

  public void StopAttack()
  {
    foreach (var ability in _abilitys)
      ability.StopAbility();
  }
  
  public void AnimateDie() => _animator.Play("Die");
  
  public void AnimateVictory() => _animator.Play("VictoryStart");

  public IEnumerable<Ability> Abilitys => _abilitys;

  public void SetState(string state, bool isActive)
  {
    if (isState && isActive)
      return;

    if (isActive)
      StopMainAttack();
    else
      StartMainAttack();

    isState = isActive;
    _animator.SetBool(state, isActive);
  }

  private void CreateAbility(AbilityData abilityData)
  {
    Ability ability = null;
    switch (abilityData.AbilityType)
    {
      case AbilityType.BallAttackAbility:
        ability = gameObject.AddComponent<BallAttackAbility>();
        break;
      case AbilityType.ShieldAbility:
        ability = gameObject.AddComponent<ShieldAbility>();
        break;
      case AbilityType.FreezeAbility:
        ability = gameObject.AddComponent<FreezeAbility>();
        break;
      case AbilityType.CometAbility:
        ability = gameObject.AddComponent<CometAbility>();
        break;
    }
    ability.Initialize(abilityData.Time, abilityData.CooldownTime, _enemy);
    _abilitys.Add(ability);
  }

  protected virtual void OnTriggerEnter(Collider collider)
  {
    OnHit?.Invoke();
    //_animator.SetTrigger("GetHit");
  }
  
  public void Hit() => OnHit?.Invoke();
  
  private void SetMainAbility(Ability ability) => _mainAbility = ability;

  private void StartMainAttack() => _mainAbility?.StartAbility();
  
  private void StopMainAttack() =>_mainAbility?.StopAbility();

}
